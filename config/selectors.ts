type selectorStr = {
    [key: string]: string
}

export const selector: selectorStr = {
	//task1
	mainSearch : '[placeholder="Найти вопрос, ответ, тег или пользователя"]',
	seleniumTheme : 'span=Selenium',
	questions : '[href="https://qna.habr.com/tag/selenium/questions"]',
	questionsList : '[class="content-list"]',
	question3 : '//li[3]/div/div[1]/div/h2/a',
	seleniumTag : '[href="https://qna.habr.com/tag/selenium"]',
	navBar : '[class="btn btn_navbar_toggle"]',
	navBarQuestions : '#item_questions',
	twitter : '[title="Наш аккаунт в Twitter"]'
}