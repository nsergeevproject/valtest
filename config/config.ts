type confStr = {
    [key: string]: string
}
const config: confStr = {
	task1  : 'https://qna.habr.com/',
	swapi : 'https://swapi.dev/api/'
}

export {config}