import { When } from '@cucumber/cucumber'
import { selector } from '../../config/selectors'

When(/^wait until (.*) condition (.*) for (.*) equal (.*)$/, async (elem: string, cond: string, attr: string, value: string) => {

	switch (cond) {
	case 'title': {
		await browser.waitUntil(async () => {
			return (await browser.getTitle() == elem)
		})
		break
	}
	case 'value': {
		await browser.waitUntil(async () => {
			const element = await $(selector[elem])
			return (await element.getValue() == value)
		})
		break
	}
	case 'exist': {
		await browser.waitUntil(async () => {
			if (Object.prototype.hasOwnProperty.call(selector, elem)) {
				const element = await $(selector[elem])
				return element.isExisting()
			}
			else {
				const element = await $(attr + '*=' + value)
				return element.isExisting()
			}
		})
		break
	}
	case 'displayed': {
		await browser.waitUntil(async () => {
			if (Object.prototype.hasOwnProperty.call(selector, elem)) {
				const element = await $(selector[elem])
				return element.isDisplayed()
			}
			else {
				const element = await $(attr + '*=' + value)
				return element.isDisplayed()
			}
		})
		break
	}
	case 'nodisplayed': {
		await browser.waitUntil(async () => {
			if (Object.prototype.hasOwnProperty.call(selector, elem)) {
				const element = await $(selector[elem])
				return ((await element.isDisplayed()) == false)
			}
			else {
				const element = await $(attr + '*=' + value)
				return ((await element.isDisplayed()) == false)
			}
		})
		break
	}
	case 'contained': {
		await browser.waitUntil(async () => {
			const list = await $(selector[elem])
			const element = await list.$(attr + '*=' + value)
			return element.isDisplayed()
		})
		break
	}
	case 'text': {
		await browser.waitUntil(async () => {
			const element = await $(selector[elem])
			return (await element.getText() == value)
		})
		break
	}
	case 'notexist': {
		await browser.waitUntil(async () => {
			if (Object.prototype.hasOwnProperty.call(selector, elem)) {
				const element = await $(selector[elem])
				return ((await element.isExisting()) == false)
			}
			else {
				const element = await $(attr + '*=' + value)
				return ((await element.isExisting()) == false)
			}
		})
		break
	}
	}
})