import { When } from '@cucumber/cucumber'
import axios from 'axios'
import { config } from '../../config/config'

When(/^Check that all ships of this (.*) have a link to it$/, async (searchHero: string) => {
	let data = await axios.get(config.swapi + 'people')
	let next = data.data.next
	let peopleList = data.data.results
	let Hero = await peopleList.filter(function(human) {
		return human.name == searchHero;
	  });

	while (Hero.length == 0 && next != null)
	{
		data = await axios.get(next)
		next = data.data.next
		peopleList = data.data.results

			Hero = await peopleList.filter(function(human) {
				return human.name == searchHero;
	  		});
	}
	if(Hero.length == 0){
		return console.log('Hero not found')
	}

	const heroId = Hero[0].url
	const heroStarships = Hero[0].starships

	console.log(`ships found = ${heroStarships.length}`)

	const linkCheck = await Promise.all(heroStarships.map(async function(ship) {
		const checkShip = await axios.get(ship)
		const pilots = checkShip.data.pilots
		const linkpilot = pilots.filter(link => link == heroId);
		return linkpilot == heroId ? 'link' : 'unlink'
	  }));

	console.log(linkCheck)
})