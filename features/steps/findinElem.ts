import { When } from '@cucumber/cucumber'
import { selector } from '../../config/selectors'

When(/^find in (.*) element (.*)$/, async (list : string, elem : string) => {

	const qlist = await $(selector[list])

	const link = await qlist.$(selector[elem])
	await link.waitForExist()
	await link.waitForClickable()
	await link.click()
})
