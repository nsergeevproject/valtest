import { When } from '@cucumber/cucumber'
import { selector } from '../../config/selectors'

When(/^enter (.*) to (.*)$/, async (text, elem) => {
	const element = await $(selector[elem])
	await element.waitForExist()
	await element.waitForClickable()
	await element.click()
	await element.setValue(text)
})