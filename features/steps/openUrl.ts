import { Given } from '@cucumber/cucumber'
import { config } from '../../config/config'

Given(/^open URL ([^"]*)$/, (URL: string) => {
	browser.url(config[URL])
})