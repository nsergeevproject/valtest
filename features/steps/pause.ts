import { When } from '@cucumber/cucumber'

When(/^pause (.*)$/, async (pauseTime: number) => {
	await browser.pause(pauseTime)
})