import { When } from '@cucumber/cucumber'
import { selector } from '../../config/selectors'

When(/^click element (.*)$/, async (elem: string) => {
	const element = await $(selector[elem])
	await element.waitForExist()
	await element.waitForClickable()
	await element.click()
})